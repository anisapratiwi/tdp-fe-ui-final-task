import './assets/css/style.css';

// assets image
import banner from './assets/img/architect.jpg';
import house2 from './assets/img/house2.jpg';
import house3 from './assets/img/house3.jpg';
import house4 from './assets/img/house4.jpg';
import house5 from './assets/img/house5.jpg';
import map from './assets/img/map.jpg';
import team1 from './assets/img/team1.jpg';
import team2 from './assets/img/team2.jpg';
import team3 from './assets/img/team3.jpg';
import team4 from './assets/img/team4.jpg';

function App() {
  return (
    <div className="App">

      {/* Navbar (sit on top) */}
      
        <div className='navbar'>
          <a href="/header" className='navbar-item-left'><b>EDTS</b> TDP Batch #2</a>
          <a href="/#contact" className='navbar-item-right a-hover'>Contact</a>
          <a href="/#about" className='navbar-item-right a-hover'>About</a>
          <a href="/#projects" className='navbar-item-right a-hover'>Projects</a>
        </div>
     

      
      {/* Header */}
      <header >        
          <img src={banner} alt="Architecture" className='responsive-img' />
          <h1>
              <b>EDTS</b> <span>Architects</span>
          </h1>
          {/* <div>
            
          </div>         */}
      </header>
      

      {/* Page content */}
      <div className="container ">

        {/* Project Section */}
        <div id="projects" className='jarak-section'>
          <h3>Projects</h3>
          <hr></hr>

        </div>

        <div className='img-container '>
          <div className='img-gallery'>
            <div className='title-img'>Summer House</div>
            <img src={house2} alt="House" />
          </div>

          <div className='img-gallery'>
            <div className='title-img'>Brick House</div>
            <img src={house3} alt="House" />
          </div>

          <div className='img-gallery'>
            <div className='title-img'>Renovated</div>
            <img src={house4} alt="House" />
          </div>

          <div className='img-gallery'>
            <div className='title-img'>Barn House</div>
            <img src={house5} alt="House" />
          </div>

          <div className='img-gallery'>
            <div className='title-img'>Summer House</div>
            <img src={house3} alt="House" />
          </div>
          
          <div className='img-gallery'>
            <div className='title-img'>Brick House</div>
            <img src={house2} alt="House" />
          </div>

          <div className='img-gallery'>
            <div className='title-img'>Renovated</div>
            <img src={house5} alt="House" />
          </div>

          <div className='img-gallery'>
            <div className='title-img'>Barn House</div>
            <img src={house4} alt="House" />
          </div>

        </div>
        

        {/* About Section */}
        <div className='jarak-section' id='about'>
          <h3>About</h3>
          <hr></hr>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
            magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Excepteur sint
            occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum consectetur
            adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco
            laboris nisi ut aliquip ex ea commodo consequat.
          </p>
        </div>
        <div className='img-container aboute-img '>
          <div className='img-gallery' >
            <img src={team1} alt="Jane" />
            <h3>Jane Doe</h3>
            <p className='job-title'>CEO &amp; Founder</p>
            <p>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
            <p><button className='btn-contact'>Contact</button></p>
          </div>
          <div className='img-gallery' >
            <img src={team2} alt="John" />
            <h3>John Doe</h3>
            <p className='job-title'>Architect</p>
            <p>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
            <p><button className='btn-contact'>Contact</button></p>
          </div>
          <div className='img-gallery' >
            <img src={team3} alt="Mike" />
            <h3>Mike Ross</h3>
            <p className='job-title'>Architect</p>
            <p>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
            <p><button className='btn-contact'>Contact</button></p>
          </div>
          <div className='img-gallery' >
            <img src={team4} alt="Dan" />
            <h3>Dan Star</h3>
            <p className='job-title'>Architect</p>
            <p>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
            <p><button className='btn-contact'>Contact</button></p>
          </div>
        </div>

        {/* Contact Section */}
        <div id="contact" className='jarak-section'>
          <h3>Contact</h3>
          <hr></hr>
          <p>Lets get in touch and talk about your next project.</p>

          <form action="#">
            <div className='fields'>
              <input className='textfield' type="text" name="name" id="name" placeholder="Name" required />
            </div>
            <div className='fields'>
              <input className='textfield' type="email" name="email" id="email" placeholder="Email" required />
            </div>
            <div className='fields'>
              <input className='textfield' type="text" name="subject" id="subject" placeholder="Subject" required />
            </div>
            <div className='fields'>
              <input className='textfield' type="text" name="comment" id="comment" placeholder="Comment" required />
            </div>

            <div className='send-btn-wrapper'>
              <input type="submit" value="SEND MESSAGE" className='send-btn' />
            </div>
          </form>
          

          {/***  
           add your element form here 
           ***/}

        </div>

        {/* Image of location/map */}
        <div className='jarak-section'>
          <img src={map} alt="maps" className='responsive-img'/>
        </div>

        <div className='jarak-section'>
          <h3>Contact List</h3>
          <hr></hr>

          <table className='table-contact-list'>
            <tr>
              <th>Name</th>
              <th>Email</th>
              <th>Country</th>
            </tr>
            <tr>
              <td>Alfreds Futterkiste</td>
              <td>alfreds@gmail.com</td>
              <td>Germany</td>
            </tr>
            <tr>
              <td>Reza</td>
              <td>reza@gmail.com</td>
              <td>Indonesia</td>
            </tr>
            <tr>
              <td>Ismail</td>
              <td>ismail@gmail.com</td>
              <td>Turkey</td>
            </tr>
            <tr>
              <td>Holand</td>
              <td>holand@gmail.com</td>
              <td>Belanda</td>
            </tr>
          </table>

          {/***  
           add your element table here 
           ***/}

        </div>

        {/* End page content */}
      </div>

      {/* Footer */}
      <footer>
        <p>Copyright 2022</p>
      </footer>

    </div>
  );
}

export default App;
